﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code;
using UnityEngine;

[RequireComponent(typeof(KeyAction))]
public class DinoNeck : MonoBehaviour
{
    public float speed;
    public float position;
    
    private float target;
    private KeyAction KeyAction;

    private void Awake()
    {
        KeyAction = GetComponent<KeyAction>();
    }

    void Start()
    {
        position = 0;
        target = 0;
    }
    
    void Update()
    {
        if (KeyAction.Pressed())
        {
            target = 1 - target;
        }
        if (!Mathf.Approximately(position, target))
        {
            position += Time.deltaTime * speed * Math.Sign(target - position);
            position = Mathf.Clamp(position, 0, 1);
        }
    }
}
