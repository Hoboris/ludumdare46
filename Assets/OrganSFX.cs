﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrganSFX : MonoBehaviour
{
    public AudioClip[] organSFXclips;
    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Play(AudioClip clip)
    {
        audioSource.clip = clip;
        audioSource.Play();
    }
}
