﻿using Code;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
   public static SoundManager Instance;

   private OrganSFX OrganSfx;
   private Music music;

   public float maxLowPass;
   public float minLowPass;
   
   private void Awake()
   {
      Instance = this;
      OrganSfx = GetComponentInChildren<OrganSFX>();
      music = GetComponentInChildren<Music>();
   }

   public void PlayOrganSFX(AudioClip clip)
   {
      OrganSfx.Play(clip);
   }

   public void AdjustMusicFilter(float speed)
   {
      var value = 0f;

      value = minLowPass + speed * (maxLowPass - minLowPass);
      
      music.ApplyLowPass(value);
   }
}
