%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Turbosaurus_Jaw_Mask_00
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: C_turbosaurus_ultimate_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/L_shoulder_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/L_shoulder_jnt/L_elbow_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/L_shoulder_jnt/L_elbow_jnt/L_wrist_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/L_shoulder_jnt/L_elbow_jnt/L_wrist_jnt/L_wrist_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/R_shoulder_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/R_shoulder_jnt/R_elbow_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/R_shoulder_jnt/R_elbow_jnt/R_wrist_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/R_shoulder_jnt/R_elbow_jnt/R_wrist_jnt/R_wrist_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/C_neck_00_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/C_neck_00_jnt/C_neck_01_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/C_neck_00_jnt/C_neck_01_jnt/C_neck_02_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/C_neck_00_jnt/C_neck_01_jnt/C_neck_02_jnt/C_neck_03_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/C_neck_00_jnt/C_neck_01_jnt/C_neck_02_jnt/C_neck_03_jnt/C_neck_04_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/C_neck_00_jnt/C_neck_01_jnt/C_neck_02_jnt/C_neck_03_jnt/C_neck_04_jnt/C_head_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_chest_jnt/C_neck_00_jnt/C_neck_01_jnt/C_neck_02_jnt/C_neck_03_jnt/C_neck_04_jnt/C_head_jnt/C_jaw_jnt
    m_Weight: 1
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt/L_ankleUp_bis_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt/L_toeBig_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt/L_toeBig_jnt/L_toeBig_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt/L_toeMiddle_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt/L_toeMiddle_jnt/L_toeMiddle_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt/L_toeSmall_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/L_hip_jnt/L_knee_jnt/L_ankleUp_jnt/L_ankleDown_jnt/L_toeSmall_jnt/L_toeSmall_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt/R_ankleUp_bis_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt/R_toeBig_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt/R_toeBig_jnt/R_toeBig_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt/R_toeMiddle_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt/R_toeMiddle_jnt/R_toeMiddle_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt/R_toeSmall_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/R_hip_jnt/R_knee_jnt/R_ankleUp_jnt/R_ankleDown_jnt/R_toeSmall_jnt/R_toeSmall_end_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt/C_tail_01_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt/C_tail_01_jnt/C_tail_02_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt/C_tail_01_jnt/C_tail_02_jnt/C_tail_03_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt/C_tail_01_jnt/C_tail_02_jnt/C_tail_03_jnt/C_tail_04_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt/C_tail_01_jnt/C_tail_02_jnt/C_tail_03_jnt/C_tail_04_jnt/C_tail_05_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt/C_tail_01_jnt/C_tail_02_jnt/C_tail_03_jnt/C_tail_04_jnt/C_tail_05_jnt/C_tail_06_jnt
    m_Weight: 0
  - m_Path: C_turbosaurus_ultimate_jnt/C_pelvis_jnt/C_tail_00_jnt/C_tail_01_jnt/C_tail_02_jnt/C_tail_03_jnt/C_tail_04_jnt/C_tail_05_jnt/C_tail_06_jnt/C_tail_end_jnt
    m_Weight: 0
  - m_Path: Dino_LessLow
    m_Weight: 0
