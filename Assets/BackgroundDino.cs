﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundDino : MonoBehaviour
{
    private Animator Dino;
    private MovementController m;
    private MovingObject o;
    private static readonly int SpeedParam = Animator.StringToHash("Speed");
    private void Awake()
    {
        o = GetComponent<MovingObject>();
        m = FindObjectOfType<MovementController>();
        Dino = GetComponent<Animator>();
    }

    private void Update()
    {
        Dino.SetFloat(SpeedParam, m.NormalizeSpeed(o.Speed));
    }
}
