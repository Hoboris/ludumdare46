using UnityEngine;

namespace Code
{
    public abstract class ActionReleaser : MonoBehaviour
    {
        public Action Action;

        protected Brain brain;

        private void Start()
        {
            brain = FindObjectOfType<Brain>();
        }

        protected void SendAction()
        {
            brain.SendAction(Instantiate(Action));
        }
    }
}