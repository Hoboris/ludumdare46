﻿using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.UI
{
    public class DinoUIManager : MonoBehaviour
    {
        // KEY PRESSES
        [SerializeField] private GameObject KeyPressWrapperTemplate;
        [SerializeField] private GameObject BrainNeedWrapperTemplate;
        [SerializeField] private KeyAction[] keyActions;
        [SerializeField] private Vector3 keyPressWrapperOffset;
        [SerializeField] private Vector3 brainNeedWrapperOffset;
        
        // NEEDS
        private Brain leBrain;
        public float dist;
        
        // FEED
        private UIFeed feed;
        
        //Tutorial
        private Tutorial tuto;

        private void Awake()
        {
            keyActions = FindObjectsOfType<KeyAction>();
            leBrain = FindObjectOfType<Brain>();
            feed = FindObjectOfType<UIFeed>();
            tuto = FindObjectOfType<Tutorial>();
        }

        private void Start()
        { 
           Random.InitState(42);
           SetupKeyPressWrappers();
           leBrain.OnNeedSent.AddListener(DisplayNeed);
        }

        public void DisplayNeed(Need need)
        {
            var needWrapper = InstantiateBrainNeed(need.sprite);
            need.OnDeathEvent.AddListener(_ =>
            {
                feed.DisplayMessage(need);
                needWrapper.GetComponentInChildren<BrainNeedBubbleFeedback>().DestroyBubble();
            });
        }

        public void SetupKeyPressWrappers()
        {
            for (int i = 0; i < keyActions.Length; i++)
            {
                var k = keyActions[i];
                
                InstantiateKeyPressWrapper(k, k.KeyCode.ToString());
            }
        }

        public void InstantiateKeyPressWrapper(KeyAction organInput, string labelText)
        {
            var wrapperGO = Instantiate(KeyPressWrapperTemplate, organInput.transform.position + keyPressWrapperOffset, Quaternion.identity);

            var followPosition = wrapperGO.AddComponent<FollowPosition>();
            followPosition.target = organInput.transform;
            followPosition.offset = keyPressWrapperOffset;

            var lookAt = wrapperGO.AddComponent<LookAt>();
            lookAt.target = Camera.main.transform;
            
            var wrapperScript = wrapperGO.GetComponentInChildren<UIWrapper>();

            var feedbackScript = wrapperGO.GetComponentInChildren<OrganInputFeedback>();
            
            organInput.OnKeyPressed.AddListener(feedbackScript.InputPressedFeedback);
            
            wrapperScript.SetWrapper(labelText);
            
            organInput.StateChange.AddListener(oi =>
            {
                var color = oi.Enabled ? Color.white : Color.gray; 
                wrapperScript.SetWrapper(labelText, color);
            });
        }  
        
        public GameObject InstantiateBrainNeed(Sprite spr)
        {
            var w = Instantiate(BrainNeedWrapperTemplate, leBrain.transform.position + brainNeedWrapperOffset, Quaternion.identity, leBrain.transform);
            w.GetComponentInChildren<UIWrapper>().SetWrapper(spr);

            var lookAt = w.AddComponent<LookAt>();
            lookAt.target = Camera.main.transform;
            
            
            Vector2 randPos;
            randPos.x = Random.Range(-1f, 0f);
            randPos.y = Random.Range(-1f, 1f);
            randPos.Normalize();

            w.transform.DOLocalMove(new Vector3(randPos.x * dist, randPos.y * dist, transform.position.z + brainNeedWrapperOffset.z), 1f);

            return w;
        }
    }
}