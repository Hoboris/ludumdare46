namespace Code.Heart
{
    public class NeedBeat : TimeNeed
    {
        public override bool ClearedBy(Action a)
        {
            return a is Beat;
        }

        public override void OnExpiration()
        {
        }
    }
}