using UnityEngine;

namespace Code
{
    public abstract class NeedStimuli : MonoBehaviour
    {
        public Need Need;


        private Brain brain;
        private void Start()
        {
            brain = FindObjectOfType<Brain>();
        }

        protected void SendNeed()
        {
            brain.SendNeed(Instantiate(Need, gameObject.transform));
        }
    }
}