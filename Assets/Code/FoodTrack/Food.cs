using UnityEngine;

namespace Code.FoodTrack
{
    public class Food : MonoBehaviour
    {
        public bool Eaten;
        
        public void Eat()
        {
            Eaten = true;
            var moving = GetComponent<MovingObject>();
            if (moving != null)
            {
                Destroy(moving);
            }
        }
    }
}