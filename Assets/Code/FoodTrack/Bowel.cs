using System;
using System.Collections.Concurrent;
using UnityEngine;

namespace Code.FoodTrack
{
    [RequireComponent(typeof(KeyAction))]
    public class Bowel : MonoBehaviour, FoodTrack
    {
        public GameObject PoopPrefab;
        public int KeyPressesToDigest;
        private KeyAction KeyAction;
        
        
        private Food ToDigest;
        private int CurrentPresses;

        private void Awake()
        {
            KeyAction = GetComponent<KeyAction>();
        }

        private void Start()
        {
            KeyAction.Disable();
        }

        private void Update()
        {
            if (ToDigest != null && KeyAction.Pressed())
            {
                CurrentPresses++;
                if (CurrentPresses == KeyPressesToDigest)
                {
                    Destroy(ToDigest.gameObject);
                    Instantiate(PoopPrefab);
                    ToDigest = null;
                    KeyAction.Disable();
                }
            }
        }

        public bool Pass(Food food)
        {
            if (ToDigest != null) return false;

            ToDigest = food;
            food.gameObject.transform.SetParent(transform);
            food.gameObject.transform.position = transform.position;
            CurrentPresses = 0;
            KeyAction.Enable();
            return true;
        }
    }
}