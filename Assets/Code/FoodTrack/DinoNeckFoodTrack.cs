﻿using System;
using System.Collections;
using System.Collections.Generic;
using Code.FoodTrack;
using UnityEngine;

public class DinoNeckFoodTrack : MonoBehaviour, FoodTrack
{ 
    public float posistionInTrack;

    private Food InTrack;
    private Stomach Stomach;
    private DinoNeck neck;
    
    private void Awake()
    {
        neck = FindObjectOfType<DinoNeck>();
        Stomach = FindObjectOfType<Stomach>();
    }

    public bool Pass(Food food)
    {
        if (InTrack != null) return false;
        
        InTrack = food;
        food.gameObject.transform.SetParent(transform);
        food.gameObject.transform.position = transform.position;
        posistionInTrack = 0;
        return true;
    }

    // Update is called once per frame
    void Update()
    {
        if (InTrack == null) return;

        var direction = neck.position < 0.5 ? 1 : -1;
        posistionInTrack += Time.deltaTime * 0.3f * direction;

        posistionInTrack = Mathf.Clamp(posistionInTrack, 0, 1);
        if (Mathf.Approximately(posistionInTrack, 1))
        {
            if (Stomach.Pass(InTrack))
            {
                InTrack = null;
            }
        }
    }
}
