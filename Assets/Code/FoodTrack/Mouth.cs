using System;
using System.Linq;
using UnityEngine;

namespace Code.FoodTrack
{
    [RequireComponent(typeof(KeyAction))]
    public class Mouth : MonoBehaviour
    {
        public float range;

        private Food InMouth;
        private KeyAction KeyAction;
        private DinoNeckFoodTrack NeckFoodTrack;

        private bool _Opened;
        public bool Opened => _Opened;
        
        private void Awake()
        {
            KeyAction = GetComponent<KeyAction>();
            NeckFoodTrack = FindObjectOfType<DinoNeckFoodTrack>();
            _Opened = false;
        }

        private void Update()
        {
            if (!_Opened && KeyAction.Pressed())
            {
                _Opened = true;
                if (InMouth == null)
                {
                    var food = FindObjectsOfType<Food>()
                        .Where(f => !f.Eaten && Vector3.Distance(f.gameObject.transform.position, transform.position) <
                                    range)
                        .OrderBy(f => Vector3.Distance(f.gameObject.transform.position, transform.position))
                        .FirstOrDefault();

                    if (food != null)
                    {
                        food.Eat();
                        food.gameObject.transform.SetParent(transform);
                        food.gameObject.transform.position = transform.position;
                        food.gameObject.transform.localScale = new Vector3(0.4f,0.4f,0.4f);
                        InMouth = food;
                    }
                }
                else if(NeckFoodTrack.Pass(InMouth))
                {
                    InMouth = null;
                }
            }
            else
            {
                _Opened = false;
            }
        }
    }
}