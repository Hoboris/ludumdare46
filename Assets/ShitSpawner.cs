﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShitSpawner : MonoBehaviour
{
    public GameObject Prefab;

    public float Period;
    private float dt;

    public float MinRange = 0.5f;
    public float MaxRange = 3f;
    public float MinSpeed = -1;
    public float MaxSpeed = -1;

    private void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        dt += Time.deltaTime;
        if (dt > Period)
        {
            dt -= Period;
            var g = Instantiate(Prefab, gameObject.transform);
            g.SetActive(true);
            var scale = Random.Range(MinRange, MaxRange);
            g.transform.localScale = new Vector3(scale,scale,scale);
            var m = g.GetComponent<MovingObject>();
            m.Speed = Random.Range(MinSpeed, MaxSpeed);
        }
    }
}