﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class DinoNeckAnimatorPivot : MonoBehaviour
{
    private DinoNeck neck;
    private Animator Dino;
    private static readonly int NeckParam = Animator.StringToHash("HeadHeight");

    private void Awake()
    {
        neck = GetComponent<DinoNeck>();
        Dino = FindObjectOfType<Animator>();
    }

    private void Update()
    {
        Dino.SetFloat(NeckParam, neck.position);
    }
}
