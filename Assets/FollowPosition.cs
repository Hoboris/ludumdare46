﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPosition : MonoBehaviour
{
    public Transform target;
    public Vector3 offset = new Vector3();
    
    private void Update()
    {
        transform.position = target.position + offset;
    }
}
