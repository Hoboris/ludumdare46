﻿using UnityEngine;

public class TimeScore : MonoBehaviour
{
    public TimeScoreCounter counter;
    
    public int score = 0;

    private float timer;

    public bool isGameOver;

    private void Awake()
    {
        counter = FindObjectOfType<TimeScoreCounter>();
    }

    void Update()
    {
        if (isGameOver)
        {
            return;
        }
        
        timer += Time.deltaTime;

        if (timer > 1f)
        {
            timer = 0;
            score++;
            counter.UpdateScore(score);
        }
    }
}
