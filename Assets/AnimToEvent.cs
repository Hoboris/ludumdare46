﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimToEvent : MonoBehaviour
{
    public UnityEvent OnAnimEvent;

    public void AnimEventCallback()
    {
        OnAnimEvent.Invoke();
    }
}
