﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float MIN_SPEED = 0;
    public float MAX_SPEED = 10;
    public float BASE_SPEED = 1;
    
    public float Speed;
    private void Awake()
    {
        Speed = BASE_SPEED;
    }

    private void Update()
    {
        SoundManager.Instance.AdjustMusicFilter(NormalizedSpeed);
    }

    [SerializeField]
    public float NormalizedSpeed => NormalizeSpeed(Speed);

    public float NormalizeSpeed(float speed)
    {
        return (speed - MIN_SPEED) / (MAX_SPEED - MIN_SPEED);
    }

    public void SpeedUp(float amount)
    {
        Speed = Math.Min(Speed + amount, MAX_SPEED);
    }
    
    public void SpeedDown(float amount)
    {
        Speed = Math.Max(Speed  - amount, MIN_SPEED);
    }
}
